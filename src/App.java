import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

    private static final int INCORRECT_GUESS_LIMIT = 10;

    public static void main(String[] args) {
        List<String> movies = loadMovies();

        if (!movies.isEmpty()) {
            Scanner scanner = new Scanner(System.in);
            Game game = new Game(movies);

            do {
                System.out.println("You are guessing:" + game.getHint().toUpperCase());
                System.out.printf("You have guessed (%d) wrong letters: %s\n", game.getFailsCount(), game.getFailedLetters().toUpperCase());
                System.out.print("Guess a letter:");
                game.guess(scanner.nextLine());
            } while(game.getFailsCount() < INCORRECT_GUESS_LIMIT && !game.isGuessed());

            if (game.isGuessed()) {
                System.out.println("You win!");
                System.out.printf("You have guessed '%s' correctly.\n", game.getMovie().toUpperCase());
            } else {
                System.out.println("Game over. You lost!");
                System.out.printf("The correct answer would have been '%s'.", game.getMovie().toUpperCase());
            }
        }
    }

    public static List<String> loadMovies() {
        try {
            return Files.readAllLines(Paths.get("resources/movies.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}

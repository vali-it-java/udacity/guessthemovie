import java.util.ArrayList;
import java.util.List;

public class Game {

    private String movie;
    private int failsCount = 0;
    private List<String> failedCharacters = new ArrayList<>();
    private String[] allCharacters;
    private String[] guessedCharacters;

    public Game(List<String> movies) {
        movie = movies.get((int) (Math.random() * movies.size()));
        allCharacters = movie.split("");
        guessedCharacters = "_".repeat(movie.length()).split("");
    }

    public String getMovie() {
        return movie;
    }

    public String getHint() {
        return String.join("", guessedCharacters);
    }

    public int getFailsCount() {
        return failsCount;
    }

    public String getFailedLetters() {
        return String.join(" ", failedCharacters);
    }

    public void guess(String character) {
        boolean characterFound = false;

        for (int i = 0; i < allCharacters.length; i++) {
            if (allCharacters[i].equalsIgnoreCase(character)) {
                characterFound = true;
                guessedCharacters[i] = character;
            }
        }

        if (!characterFound) {
            failsCount++;
            failedCharacters.add(character);
        }
    }

    public boolean isGuessed() {
        return movie.equals(getHint());
    }
}
